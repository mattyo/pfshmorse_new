
autowatch = 1;

outlets = 2;

var MAX_CHARS = 44;
var idx = 0;
var MAX_LINES = 12;
var polyIdx = MAX_LINES;
var offsetIdx = 0;
var endOfLine = false;
var fullPage = false;
var lineCount = 0; // I don't even use this variable!
var lineString = String();
var sliceString = String();

messnamed("poly_count", polyIdx);

var p = this.patcher;


function loadbang(){
	init();
}

function init(){
	idx = 0;
	polyIdx = MAX_LINES;
	offsetIdx = 0;
	endOfLine = false;
	fullPage = false;
	isLastLetter = false;
	lineCount = 0;
	lineIdx = 0;
	// lineString = String();
 // 	sliceString = String();
 	lineString = []//= " ";
 	sliceString = []//= " ";



	messnamed("next_letter", "target", 0);
	messnamed("next_letter", "text");
	messnamed("next_letter", "target", MAX_LINES+1);
	messnamed("next_letter", "text");
	messnamed("num_roman_lines", MAX_LINES);
	messnamed("num_chars", MAX_CHARS);			//do I need this?
	messnamed("poly_count", MAX_LINES);
	messnamed("offset_index", 0);
}


function start(){
	get_new_line();
}

function add_line(str){

	//post("\n str 0: ", str[0]);
	outlet(0, "to_morse", "add_line", str);
	endOfLine = false;
	sliceString.length = 0;
	lineString = str;
	lineCount = lineString.length + 1;
	//post("\n ADD_line:", str, " ",lineCount);
}




function bang(){  //getting next letter function

	//post("\nbang: LEN: ", sliceString.length, "idx: ", idx, "string ", sliceString);

	if (idx >= sliceString.length) {

		if (endOfLine) {    //meaning end of chunk, not visual line
			//post("\n main bang: EOL");

			rotate_morse();
			get_new_line();

			return;
		}

		idx = 0;			//reset index
		polyIdx--;					//move to next line

		if (fullPage) {
			testPolyIndex();
			messnamed("poly_count", polyIdx);
			// not happening here

			offsetIdx++;
			testOffsetIndex();

		//	post("\nbang:  FULL PAGE OFFSET " + offsetIdx);
			messnamed("offset_index", offsetIdx);
			slice_it();
		}

		else if (polyIdx < 1 && fullPage == false) {     		//if we are past the last free poly
			fullPage = true;

			polyIdx = MAX_LINES;
			messnamed("poly_count", polyIdx);		//send the write pointer back to the first one
			//not here either

			offsetIdx++;
			//post("\nbang:  NON-FULL PAGE OFFSET " + offsetIdx);

				if(offsetIdx >= MAX_LINES){
					offsetIdx = 0;
				}
			messnamed("offset_index", offsetIdx);
			slice_it();
		}

		else {
		messnamed("poly_count", polyIdx);
		//ok here too
		slice_it();
		}


	}
		//post("\nmain bang: next ", sliceString[idx]);

		outlet(0, "next", sliceString[idx]);

		idx++;
}


function slice_it(){

	//post("\nSLICE IT: string length ", lineString.length);


	if(lineString.length>MAX_CHARS){
		var cutPoint = lineString.lastIndexOf(" ", MAX_CHARS);
		sliceString = lineString.slice(0, cutPoint+1);
		var extraString = lineString.slice(cutPoint+1, lineString.length);
		lineString = extraString;
		}

	else{
		sliceString = lineString;
		endOfLine = true;  //why is this true here?
		}

		//post("\nlength<MAX CHARS");
}

function get_new_line(){

	outlet(0, "add");
}

function first_line(){
	outlet(0, "add");
}


	function blank(){

			outlet(0, "to_morse", "set_blank_flag", "true")
			//post("\nmain_script blank -- blank_flag true");

			polyIdx--;

		if (fullPage) {
			testPolyIndex();
			messnamed("poly_count", polyIdx);

			offsetIdx++;
			testOffsetIndex();

			//post("\nblank:  FULL PAGE OFFSET " + offsetIdx);
			messnamed("offset_index", offsetIdx);
			get_new_line();
		}

		else if (polyIdx < 1 && fullPage == false) {
			fullPage = true;
			polyIdx = MAX_LINES;
			messnamed("poly_count", polyIdx);
			offsetIdx++;
			testOffsetIndex();
			messnamed("offset_index", offsetIdx);
		//	post("\nbang:  NON-FULL PAGE OFFSET" + offsetIdx);

			get_new_line();
		}

		else {
		//	post("\nblank:  NEITHER " + offsetIdx);
			messnamed("poly_count", polyIdx);  //does this need an offset increment?
						post("\nblank:  NEITHER " + polyIdx);
			get_new_line();
		}
}

function set_max_lines(v){

		MAX_LINES = v;
}

function set_max_chars(v){

		MAX_CHARS = v;
}


function testOffsetIndex(){

		if(offsetIdx >= MAX_LINES){
			offsetIdx = 0;
			}
}

function testPolyIndex(){

		if(polyIdx < 1){
			polyIdx = MAX_LINES;
			}
}

function rotate_morse(){
	//post("\nmain rotate_morse: rotating");
	outlet(0, "to_morse", "rotate"); //set line break for morse when roman line ends
}