function anything(){

	var tmp = arrayfromargs(messagename,arguments);

	switch(tmp[0]){

		case "_open":
			tmp[0] = "open";
			break;
		case "_float":
			tmp[0] = "float";
			break;
		case "_list":
			tmp[0] = "list";
			break;
		case "_bang":
			tmp[0] = "bang";
			break;
		case "_post":
			tmp[0] = "post";
			break;
		case "_refresh":
			tmp[0] = "refresh";
			break;
		case "_watch":
			tmp[0] = "watch";
			break;
		default: break;
		}

	str = tmp.join(' ');
	outlet(0, "add_line", str);

}