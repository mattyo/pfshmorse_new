autowatch = 1;

outlets = 2;

var ipsum = String(".-.. --- .-. . --   .. .--. ... ..- --   -.. --- .-.. --- .-.   ... .. -   .- -- . - --..--   ...- . .-. .. - ..- ...   -.-. .. ...- .. -... ..- ...   ...- .. -..-   .- -. --..--   ...- .. ...   . -   -.. . .-.. . -.-. - ..- ...   .--. . .-. .--. . - ..- .-   ... . -. ... . .-. .. - --..--   --.- ..- . --   -... .-.. .- -. -.. .. -   .... .. ...   . .. .-.-.-   --- -- -. .. ..- --   -- . -. - .. - ..- --   .- -   -. .- -- --..--   ...- .. --   --.- ..- .. -.. .- --   ... .. -. --. ..- .-.. .. ...   .- -. .-.-.-   -- . ..   . -   - .- .-.. .   .- .--. . .-. .. .-. .. --..--   - .. -- . .- --   . .-. ..- -.. .. - ..   .. ..- ...   -.-. ..- --..--   ... .- .-.. .   -.. .. ... -.-. . .-. .   .- - --- -- --- .-. ..- --   .... .. ...   . - .-.-.-   .-.. ..- -.. ..- ...   --- .-. .- - .. ---   ... . -. - . -. - .. .- .   -.-. ..- --   . - .-.-.-   -. . -.-.   .. -..   -- ..- -.-. .. ..- ...   --- -- .. - - .- --   .. -. .. -- .. -.-. ..- ... --..--   -.. . - .-. .- -..- .. -   --.- ..- .- . ... - .. ---   --.- ..- ..   .. -. .-.-.- ")
var lorem = String("Lorem ipsum dolor sit amet, veritus civibus vix an, vis et delectus perpetua senserit, quem blandit his ei. Omnium mentitum at nam, vim quidam singulis an. Mei et tale aperiri, timeam eruditi ius cu, sale discere atomorum his et. Ludus oratio sententiae cum et. Nec id mucius omittam inimicus, detraxit quaestio qui in.");

var num_morse_lines = 0;
var num_lines = 0;

function len(v){

var out = lorem.slice(0, v);

outlet(0, "target", 0);
outlet(0, "text");

for(var i=0; i<out.length;i++){
	outlet(0, "append", out[i]);
	}

}


function mlen(v){

var out = ipsum.slice(0, v);

outlet(0, "target", 0);
outlet(0, "text");

for(var i=0; i<out.length;i++){
	outlet(0, "append", out[i]);
	}
	
	outlet(0, "target", num_morse_lines);
//	post("\nset target: " + num_morse_lines);
}

function mlines(v){
//	post("\nmlines: " + num_morse_lines);
	num_morse_lines = v;	
	}
	
function lines(v){
	
	num_lines = v;	
	}