

autowatch = 1;

outlets = 1;


var q = 0;

var MAX_MORSE_CHARS = 34;
var MAX_MORSE_LINES = 12;

var idx = 0;

var polyIdx = MAX_MORSE_LINES;
var offsetIdx = 0;
var endOfLine = false;
var fullPage = false;
var EOL_flag = false;
var blank_flag = false;
var d = new Dict("counts");
var morseLength = 0;
var space = " ";

messnamed("morse_poly_count", polyIdx);

var p = this.patcher;


function loadbang(){
	init();
}

function init(){
	idx = 0;
	polyIdx = MAX_MORSE_LINES;
	offsetIdx = 0;
	endOfLine = false;
	fullPage = false;
	lineIdx = 0;
	messnamed("all_morse", "text");
	messnamed("next_morse", "target", MAX_MORSE_LINES);
	messnamed("num_morse_lines", MAX_MORSE_LINES);
	messnamed("num_morse_chars", MAX_MORSE_CHARS);
	messnamed("update_morse_lines", "bang");
	messnamed("morse_offset_index", 0);

}



function add_line(str){

	endOfLine = false;
	lineString = str;
	//post("\n add_line: ", str);
	checkLineLength();
	}



function write(glyph){


	if (EOL_flag) {
		//post("\nmorse write: EOL flag true")
	};
	//post("\nglyph ", glyph, "index ", idx);

	if( glyph == " " && idx == 0){
		//post("\n write: return index ", idx);
		return;
		}
		//post("\nwrite: after return ", idx);

	outlet(0, "append", glyph);
	idx++;
}

function word_break(){
	//post("\n------word_break------", "idx ", idx, "max ",MAX_MORSE_CHARS);

	/*for(var i = 0; i < 2; i++){
		write(" ");
		}*/

		write(space);
		write(space);

		checkLineLength();

}


function full_page_rotate(){
	//		post("\nFULL PAGE ROTATE");
			testPolyIndex();

			messnamed("morse_poly_count", polyIdx);
			offsetIdx++;

			//post("\nrotate: offset ", polyIdx);

			testOffsetIndex();

			messnamed("morse_offset_index", offsetIdx);
}

function empty_page_rotate(){     		//if we are past the last free poly

			//post("\nrotate: offset ", polyIdx);
			fullPage = true;

			polyIdx = MAX_MORSE_LINES;
			messnamed("morse_poly_count", polyIdx);		//send the write pointer back to the first one

			offsetIdx++;
			messnamed("morse_offset_index", offsetIdx);
				if(offsetIdx > MAX_MORSE_LINES){
					offsetIdx = 0;
				}
}

function testForBlankLine(){

	//post("\n morse_script test for blank: ", blank_flag);

	if (blank_flag) {

		blank();
		blank_flag = false;
	}


}

	function blank(){

			//post("\nmorse blank index ", polyIdx);
			polyIdx--;


		if (fullPage) {
			testPolyIndex();
			messnamed("morse_poly_count", polyIdx);

			offsetIdx++;
			testOffsetIndex();

			messnamed("morse_offset_index", offsetIdx);

		}

		else if (polyIdx < 1 && fullPage == false) {
			fullPage = true;
			polyIdx = MAX_MORSE_LINES;
			messnamed("morse_poly_count", polyIdx);
			offsetIdx++;
			testOffsetIndex();
			messnamed("offset_index", offsetIdx);
		//	post("\nbang:  NON-FULL PAGE OFFSET" + offsetIdx);
		}

		else {
			messnamed("morse_poly_count", polyIdx);  //does this need an offset increment?
			//post("\nblank:  NEITHER " + offsetIdx);
		}
}


function set_EOL(v){

	EOL_flag = v;
	//post("\mEOL flag", v);
}

function set_max_lines(v){

		MAX_MORSE_LINES = v;

}

function set_max_chars(v){

		MAX_MORSE_CHARS = v;
}

function set_blank_flag(v){

	if (idx == 0) {
		blank();
	}
	else{
		blank_flag = v;
	}
}

function testOffsetIndex(){

		if(offsetIdx >= MAX_MORSE_LINES){
			offsetIdx = 0;
			}
}

function testPolyIndex(){

		if(polyIdx < 1){
			polyIdx = MAX_MORSE_LINES;
			}
}

function rotate(){
		testForBlankLine();

		idx = 0;			//reset index
		polyIdx--;					//move to next line
		//post("\nmorse rotate: increment poly ", polyIdx);


		if (fullPage) {
			full_page_rotate()
		}

		if (polyIdx < 1 && fullPage == false) {
			empty_page_rotate()
		}

		messnamed("morse_poly_count", polyIdx);
		EOL_flag = false;

		idx = 0;


}

function checkLineLength(){

	morseLength = 0;
	var s = lineString.indexOf(space);
	if(s == -1){
		var nextWord = lineString;
	}
	else {
		var nextWord = lineString.slice(0, s);
	}
//	post("\nnextword: ", nextWord, " ", s);

	for (var i = 0; i < nextWord.length; i++) {
		var letterLength;
		if (nextWord[i] == '"') {
			letterLength = 6;
		}
		else{
			var letter = nextWord[i].toUpperCase();
			letterLength = parseInt(d.get(letter));
			if (isNaN(letterLength)) {
				letterLength = 1;
			};
		}
		morseLength = morseLength + letterLength + 1; //+1 is for the spaces between letters
//		post("\nmorse length accum" , i , " ", letter,": ", letterLength);
	};

//	post("\nnextword: ", nextWord, " morseLength: ", morseLength);

	if ((MAX_MORSE_CHARS - idx) < morseLength) {
//		post("\n checkLineLength says rotate");
			rotate();
	};

	lineString = lineString.slice(s+1);

}

function chopFirstWord(){

	morseLength = 0;
	var s = lineString.indexOf(space);
	var nextWord = lineString.slice(0, s);
	lineString = lineString.slice(s+1);

		//post("\nnextword: ", nextWord, " morseLength: ", morseLength);

}
