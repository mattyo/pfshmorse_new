{
	"name" : "pfshMorse_new",
	"version" : 1,
	"creationdate" : -789651906,
	"modificationdate" : -761676635,
	"viewrect" : [ 1291.0, 121.0, 300.0, 500.0 ],
	"autoorganize" : 1,
	"hideprojectwindow" : 1,
	"showdependencies" : 1,
	"autolocalize" : 1,
	"contents" : 	{
		"patchers" : 		{
			"pfshMorse_new.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1,
				"toplevel" : 1
			}
,
			"morse_lines.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"set_num_lines.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"stopwatch.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"beep.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}
,
			"jstester.maxpat" : 			{
				"kind" : "patcher",
				"local" : 1
			}

		}
,
		"code" : 		{
			"toUpper.js" : 			{
				"kind" : "javascript",
				"local" : 1
			}
,
			"main_script.js" : 			{
				"kind" : "javascript",
				"local" : 1
			}
,
			"morse_script.js" : 			{
				"kind" : "javascript",
				"local" : 1
			}
,
			"test-text.js" : 			{
				"kind" : "javascript",
				"local" : 1
			}
,
			"add-line.js" : 			{
				"kind" : "javascript",
				"local" : 1
			}
,
			"set-dodger.js" : 			{
				"kind" : "javascript",
				"local" : 1
			}

		}
,
		"data" : 		{
			"testScreenPositions.txt" : 			{
				"kind" : "textfile",
				"local" : 1
			}
,
			"pfsh.storage.json" : 			{
				"kind" : "json",
				"local" : 1
			}
,
			"pfsh.rows.json" : 			{
				"kind" : "json",
				"local" : 1
			}
,
			"map-menu.txt" : 			{
				"kind" : "textfile",
				"local" : 1
			}
,
			"pfsh.layout.storage.json" : 			{
				"kind" : "json",
				"local" : 1
			}
,
			"rate.txt" : 			{
				"kind" : "textfile",
				"local" : 1
			}
,
			"letters.json" : 			{
				"kind" : "json",
				"local" : 1
			}
,
			"Bible text part 1a.txt" : 			{
				"kind" : "textfile",
				"local" : 1
			}
,
			"morse-counts.json" : 			{
				"kind" : "json",
				"local" : 1
			}

		}

	}
,
	"layout" : 	{

	}
,
	"searchpath" : 	{

	}
,
	"detailsvisible" : 1,
	"amxdtype" : 0,
	"readonly" : 0,
	"devpathtype" : 0,
	"devpath" : "."
}
